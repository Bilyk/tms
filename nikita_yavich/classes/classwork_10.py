# Hero
# В игре есть солдаты и герои. У каждого есть уникальный номер, и свойство,
# в котором хранится принадлежность команде. У солдат есть метод
# "move_to_hero", который в качестве аргумента принимает объект типа "Hero".
# У героев есть метод увеличения собственного уровня level_up.
# В основной ветке программы создается по одному герою для каждой команды. В
# цикле генерируются объекты-солдаты. Их принадлежность команде определяется
# случайно. Солдаты разных команд добавляются в разные списки.
# Измеряется длина списков солдат противоборствующих команд и выводится на
# экран. У героя, принадлежащего команде с более длинным списком, поднимается
# уровень.
# Отправьте одного из солдат первого героя следовать за ним. Выведите на
# экран идентификационные номера этих двух юнитов.


import random


class Hero:
    level = 1
    unit_unical_number = 0

    def __init__(self, name):
        """
        Hero instance constructor.
        """
        self.name = name
        self.unit_unical_number = Hero.unit_unical_number + 1
        Hero.unit_unical_number += 1

    def level_up(self):
        """
        This method increases hero's lvl by 1.
        """
        self.level += 1

    def __str__(self):
        return f'{self.name}'


class Soldier:
    def __init__(self):
        """
        Soldier instance creation method.
        """
        self.unit_unical_number = Hero.unit_unical_number + 1
        Hero.unit_unical_number += 1


class Teams:
    teams = []

    def __init__(self, name, hero):
        """
        Team instance creation method.
        """
        self.soldiers = []
        self.hero = hero
        self.name = name
        Teams.teams.append(self)

    def __str__(self):
        return f'{self.name}'

    @staticmethod
    def who_wins():
        """
        This method counts and compares two teams armies.
        Those team who has more soldiers 'wins' and the hero of the team
        increases his level.
        """
        print(f'There are {len(Teams.teams[0].soldiers)} soldiers in'
              f' {Teams.teams[0]} and {len(Teams.teams[1].soldiers)} in'
              f' {Teams.teams[1]}.')
        if len(Teams.teams[0].soldiers) > len(Teams.teams[1].soldiers):
            Teams.teams[0].hero.level_up()
            print(f'{Teams.teams[0].hero} has just increased his level.')
        if len(Teams.teams[1].soldiers) > len(Teams.teams[0].soldiers):
            Teams.teams[1].hero.level_up()
            print(f'{Teams.teams[1].hero} has just increased his level.')
        if len(Teams.teams[1].soldiers) == len(Teams.teams[0].soldiers):
            print('No one wins.')

    def move_to_hero(self):
        """
        This method 'moves' a randomly chosen soldier to his Hero.
        """
        print(f'Soldier with unical number'
              f' {random.choice(self.soldiers)} '
              f'moved to  his hero {self.hero.name} with unical number'
              f' {self.hero.unit_unical_number}.')


def soldiers(number_of_soldiers: int):
    """
    This function creates soldiers automatically using input quantity and
    randomly chooses their team.
    """
    for i in range(number_of_soldiers):
        team = random.choice(Teams.teams)
        soldier = Soldier()
        if team == Teams.teams[0]:
            Teams.teams[0].soldiers.append(soldier.unit_unical_number)
        else:
            Teams.teams[1].soldiers.append(soldier.unit_unical_number)


# Create 2 heroes:
hero_red = Hero('hero_red')
hero_blue = Hero('hero_blue')
# Create 2 teams and make heroes from the previous step be the captains of
# these teams:
team_red = Teams('team_red', hero_red)
team_blue = Teams('team_blue', hero_blue)
# Create soldiers:
soldiers(10)
# Lets find out the winner:
Teams.who_wins()
# Lets make the random soldier from chosen team move to his hero:
team_red.move_to_hero()


# Путешествие
# Вы идете в путешествие, надо подсчитать сколько у денег у каждого студента.
# Класс студен описан следующим образом:
# class Student:
# def __init__(self, name, money):
# self.name = name
# self.money = money
# Необходимо понять у кого больше всего денег и вернуть его имя.
# assert most_money([ivan, oleg, sergei]) == "sergei"
# assert most_money([ivan, oleg]) == "ivan"
# assert most_money([oleg]) == "Oleg"

class Student:

    def __init__(self, name: str, money: int):
        """
        Student class constructor.
        """
        self.name = name
        self.money = money

    @property
    def money_getter(self):
        return self.money

    # @money_getter.setter
    # def money_setter(self, money):
    # self.money = money

    def max_money(arg: list):
        """
        Takes the students list as argument and returns the name of the
        person with biggest sum of money.
        """
        st_money = [student.money_getter for student in arg]
        max_money = max(st_money)
        if st_money.count(max_money) == len(st_money):
            return 'all'
        for i in arg:
            if i.money_getter == max_money:
                return i.name


s1 = Student('Vasya', 1000)
s2 = Student('Masha', 9000)
s3 = Student('Kirill', 1500)
if __name__ == '__main__':
    print(Student.max_money([s1, s2, s3]))
    assert Student.max_money([s1, s2, s3]) == 'Masha'


# Создать класс Store и добавить в него словарь, где будет храниться item:
# price.
# Реализовать в классе методы add_item, remove_item
# Реализовать метод overall_price_discount который считает полную сумму со
# скидкой добавленных в магазин товаров
# Реализовать метод overall_price_no_discount
# который считает сумму товаров без скидки.
# Реализовать классы GroceryStore и HardwareStore и унаследовать эти классы
# от Store. Реализовать в GraceryStore проверку, что объект, который был
# передан в класс пренадлежит классу Food HardwareStore Tools.

class Goods:
    def __init__(self, price, discount):
        """
        Goods class constructor.
        """
        self.price = price
        self.discount = discount

    @property
    def get_discount(self):
        return self.discount

    @get_discount.setter
    def set_discount(self, discount):
        self.discount = discount


class Food(Goods):
    def __init__(self, price, discount):
        """
        Food class constructor.
        """
        super().__init__(price, discount)


class Tools(Goods):
    def __init__(self, price, discount):
        """
        Tools class constructor.
        """
        super().__init__(price, discount)


class Banana(Food):
    def __init__(self, price, discount):
        """
        Banana class constructor.
        """
        super().__init__(price, discount)


class Apple(Food):
    def __init__(self, price, discount):
        """
        Apple class constructor.
        """
        super().__init__(price, discount)


class Cherry(Food):
    def __init__(self, price, discount):
        """
        Cherry class constructor.
        """
        super().__init__(price, discount)


class Hammer(Tools):
    def __init__(self, price, discount):
        """
        Hammer class constructor.
        """
        super().__init__(price, discount)


class Nails(Tools):
    def __init__(self, price, discount):
        """
        Nails class constructor.
        """
        super().__init__(price, discount)


class Axe(Tools):
    def __init__(self, price, discount):
        """
        Axe class constructor.
        """
        super().__init__(price, discount)


class Store:
    store = {}

    @staticmethod
    def add_item(args):
        """
        This method adds given goods to the store dictionary.
        """
        for i in args:
            Store.store.setdefault(i, i.price)

    @staticmethod
    def remove_item(args):
        """
        This method removes given goods from the store dictionary.
        """
        for i in args:
            Store.store.pop(i)

    def __init__(self, *args):
        """
        Store class constructor.
        """
        self.goods = args
        Store.add_item(self.goods)

    def overall_price_discount(self):
        """
        This method counts total price of goods in the Store with discount.
        """
        total_price = 0
        for i in Store.store.keys():
            total_price += (i.price * (100 - i.discount) / 100)
        print(total_price)

    def overall_price_no_discount(self):
        """
        This method counts total price of goods in the Store without discount.
        """
        total_price_no_discount = 0
        for i in Store.store.values():
            total_price_no_discount += i
        print(total_price_no_discount)


class GroceryStore(Store):

    def __init__(self, *args):
        """
        GroceryStore class constructor.
        """
        temp_list = []
        wrong_input_items = 0
        for i in args:
            for j in i.goods:
                if isinstance(j, Food):
                    temp_list.append(j)
                else:
                    wrong_input_items += 1
        if wrong_input_items != 0:
            print(f'Everything in GroceryStore must be food! Youve missed '
                  f'{wrong_input_items} item(s)!')
        self.goods = tuple(temp_list)


class HardwareStore(Store):

    def __init__(self, *args):
        """
        HardwareStore class constructor.
        """
        temp_list = []
        wrong_input_items = 0
        for i in args:
            for j in i.goods:
                if isinstance(j, Tools):
                    temp_list.append(j)
                else:
                    wrong_input_items += 1
        if wrong_input_items != 0:
            print(f'Everything in HardwareStore must be tools! Youve missed '
                  f'{wrong_input_items} item(s)!')
        self.goods = tuple(temp_list)


# Create instances of goods:
nails = Nails(100, 5)
apple = Apple(1000, 5)
banana = Banana(1500, 6)
# Crete instances of Store with goods.
store1 = Store(apple, banana, nails)
store2 = Store(nails, apple)
# Check the price of goods in store1 with and without discount:
store1.overall_price_discount()
store2.overall_price_no_discount()
# Create GroceryStore and HardwareStore instances and test Class-check func:
grocery_store1 = GroceryStore(store1)
hardware_store1 = HardwareStore(store2)
