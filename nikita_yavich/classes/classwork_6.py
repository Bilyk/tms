import datetime


# 1) Напишите функцию, которая возвращает строку: “Hello world!”


def func_1():
    return 'Hello world'


print(func_1())


# 2)Напишите функцию, которая вычисляет сумму трех чисел и возвращает
# результат в основную ветку программы.


def func_2(x, y, z):
    return x + y + z


x, y, z = map(int, input('Input 3 numbers separated by space: ').split())
print(func_2(x, y, z))


# 3)Придумайте программу, в которой из одной функции вызывается вторая.
# При этом ни одна из них ничего не возвращает в основную ветку программы
# обе должны выводить результаты своей работы с помощью функции print().


def func_3_1():
    print('func_3_1 is running')
    func_3_2()


def func_3_2():
    print('func_3_2 is running')


func_3_1()


# 4)Напишите функцию, которая не принимает отрицательные числа. и возвращает
# число наоборот.
# 21445 => 54421
# 123456789 => 987654321


def func_4(x):
    if int(x) < 0:
        print('The number schould be > 0')
    else:
        return reversed(x)


x = input('Input the number: ')
print(''.join(list(func_4(x))))


# 5)Напишите функцию fib(n), которая по данному целому неотрицательному n
# возвращает n-e число Фибоначчи.


def fib(n):
    x = 0
    y = 1
    z = x + y
    k = 3
    while k != n:
        x = y
        y = z
        z = x + y
        k += 1
    return z


n = int(input('Input number N>0 '))
if n < 0:
    n = int(input('Input number N>0 '))
print(fib(n))


# 6)Напишите функцию, которая проверяет на то, является ли строка палиндромом
# или нет.
# Палиндром — это слово или фраза, которые одинаково читаются слева
# направо и справа налево.


def poli(x: str):
    return x == ''.join(reversed(x))


x = input('input the string: ')
print(poli(x))


# 7)У вас интернет магазин, надо написать функцию которая проверяет что
# введен правильный купон и он еще действителен
# def check_coupon(entered_code, correct_code, current_date, expiration_date):
# Code here!
# check_сoupon("123", "123", "July 9, 2015", "July 9, 2015")  == True
# check_сoupon("123", "123", "July 9, 2015", "July 2, 2015")  == False


def check_coupon(entered_code, correct_code, current_date, expiration_date):
    current_date_improved = datetime.datetime.strptime(current_date,
                                                       "%B %d, %Y")
    expiration_date_improved = datetime.datetime.strptime(expiration_date,
                                                          "%B %d, %Y")
    print(
        entered_code == correct_code and
        expiration_date_improved >= current_date_improved)


entered_code, correct_code, current_date, expiration_date = ("123", "123",
                                                             "July 9, 2015",
                                                             "July 9, 2015")
check_coupon(entered_code, correct_code, current_date, expiration_date)


# 8)Фильтр. Функция принимает на вход список, проверяет есть ли эти элементы
# в списке exclude, если есть удаляет их и возвращает список с оставшимися
# элементами
# exclude = ["African", "Roman Tufted", "Toulouse", "Pilgrim", "Steinbacher"]
# filter_list(["Mallard", "Hook Bill", "African", "Crested", "Pilgrim",
# "Toulouse", "Blue Swedish"]) => ["Mallard", "Hook Bill", "Crested",
# "Blue Swedish"]
# ["Mallard", "Barbary", "Hook Bill", "Blue Swedish", "Crested"] => [
# "Mallard", "Barbary", "Hook Bill", "Blue Swedish", "Crested"]
# ["African", "Roman Tufted", "Toulouse", "Pilgrim", "Steinbacher"] => []


def filter_list(list_1):
    return [i for i in list_1 if i not in exclude]


exclude = ["African", "Roman Tufted", "Toulouse", "Pilgrim", "Steinbacher"]
list_1 = ["Mallard", "Hook Bill", "African", "Crested", "Pilgrim",
          "Toulouse", "Blue Swedish"]
print(filter_list(list_1))


# also the same:

def func(x):
    return x not in exclude


exclude = ["African", "Roman Tufted", "Toulouse", "Pilgrim", "Steinbacher"]
list_1 = ["Mallard", "Hook Bill", "African", "Crested", "Pilgrim",
          "Toulouse", "Blue Swedish"]
print(list(filter(func, list_1)))
