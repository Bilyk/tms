from collections import Counter
import string

# 1.
# Перевести строку в массив
# "Robin Singh" => ["Robin”, “Singh"]
# "I love arrays they are my favorite" =>
# ["I", "love", "arrays", "they", "are", "my", "favorite"]

str1 = "Robin Singh"
str2 = "I love arrays they are my favorite"
print('Task 1 a:', str1.split(' '))
print('Task 1 b:', str2.split(' '))


# 2.
# Дан список: [‘Ivan’, ‘Ivanou’], и 2 строки: Minsk, Belarus
# Напечатайте текст: “Привет, Ivan Ivanou! Добро пожаловать в Minsk Belarus”

list1 = ['Ivan', 'Ivanou']
str3 = 'Minsk'
str4 = 'Belarus'
print(f"Task 2: Привет, {' '.join(list1)}! Добро пожаловать в {str3} {str4}")


# 3.
# Дан список ["I", "love", "arrays", "they", "are", "my", "favorite"]
# сделайте из него строку => "I love arrays they are my favorite"

list2 = ["I", "love", "arrays", "they", "are", "my", "favorite"]
str5 = ' '.join(list2)
print('Task 3:', str5)


# 4.
# Создайте список из 10 элементов, вставьте на 3-ю позицию новое значение,
# удалите элемент из списка под индексом 6

arr2 = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j']
arr2.insert(2, "New")
del arr2[6]
print('Task 4:', arr2)


# 5.
# Есть 2 словаря
#                  a = { 'a': 1, 'b': 2, 'c': 3}
#                  b = { 'c': 3, 'd': 4,'e': “”}
# 1. Создайте словарь, который будет содержать в себе все элементы обоих
# словарей
# 2. Обновите словарь “a” элементами из словаря “b”
# 3. Проверить что все значения в словаре “a” не пустые либо не равны нулю
# 4. Проверить что есть хотя бы одно пустое значение (результат выполнения
# должен быть True)
# 5. Отсортировать словарь по алфавиту в обратном порядке
# 6. Изменить значение под одним из ключей и вывести все значения

a = {'a': 1, 'b': 2, 'c': 0}
b = {'c': 3, 'd': 4, 'e': ""}
merge = {**a, **b}
print('Task 5.1 :', merge)
a.update(b)
print('Task 5.2 :', a)
null_values = [not i for i in a.values()]
print('Task 5.3 :', all(null_values))
print('Task 5.4 :', any(null_values))
print('Task 5.5 :', sorted(a, reverse=True))
a['b'] = 20
print('Task 5.6 :', a)


# 6.
# Создать список из элементов list_a = [1,2,3,4,4,5,2,1,8]
# Вывести только уникальные значения и сохранить их в отдельную переменную
# Добавить в полученный объект значение 22
# Сделать list_a неизменяемым
# Измерить его длинну

list_a = [1, 2, 3, 4, 4, 5, 2, 1, 8]
list_b = list(set(list_a))
print('Task 6.1 :', list_b)
list_b.append(22)
print('Task 6.2 :', list_b)
list_a = tuple(list_a)
print('Task 6.3 :', len(list_a))


# Задачи на закрепление форматирования:

print('Задачи на закрепление форматирования:')

# Есть переменные a=10, b=25
# Вывести 'Summ is <сумма этих чисел> and diff = <их разница>.'
# При решении задачи использовать оба способа форматирования

a = 10
b = 25
print('1. Summ is {} and diff = {}'.format(a + b, a - b))
print(f'1. Summ is {a + b} and diff = {a - b}')


# Есть список list_of_children = [“Sasha”, “Vasia”, “Nikalai”]
# Вывести “First child is <первое имя из списка>, second is “<второе>”,
# and last one – “<третье>””

list_of_children = ["Sasha", "Vasia", "Nikalai"]
print(f"2. First child is {list_of_children[0]}, second is "
      f"{list_of_children[1]}, and last one – {list_of_children[2]}")


# *1) Вам передан массив чисел. Известно, что каждое число в этом массиве имеет
# пару, кроме одного: [1, 5, 2, 9, 2, 9, 1] => 5
# Напишите программу, которая будет выводить уникальное число

a = [1, 5, 2, 9, 2, 9, 1]
z = Counter(a).most_common()
print('1*. Unique number(s): ', end='')
for i in range(len(z)):
    if z[i][1] == 1:
        print(z[i][0], end=' ')
print("")


# *2) Дан текст, который содержит различные английские буквы и знаки препинания
# Вам необходимо найти самую частую букву в тексте. Результатом должна быть
# буква в нижнем регистре. При поиске самой частой буквы, регистр не имеет
# значения, так что при подсчете считайте, что "A" == "a". Убедитесь,
# что вы не считаете знаки препинания, цифры и пробелы, а только буквы.
# Если в тексте две и больше буквы с одинаковой частотой, тогда результатом
# будет буква, которая идет первой в алфавите. Для примера, "one" содержит
# "o", "n", "e" по одному разу, так что мы выбираем "e".
# "a-z" == "a"
# "Hello World!" == "l"
# "How do you do?" == "o"
# "One" == "e"
# "Oops!" == "o"
# "AAaooo!!!!" == "a"
# "a" * 9000 + "b" * 1000 == "a"


text = 'oooo oaaaaa ao!!!!'
text = text.lower()
text = text.replace(" ", "")
for pnct in string.punctuation:
    text = text.replace(pnct, "")
text = sorted(text)
print('2*:', Counter(text).most_common()[0][0])
