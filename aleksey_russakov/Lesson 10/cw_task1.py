# Путешествие
# Вы идете в путешествие, надо подсчитать сколько у денег у каждого студента.
# Класс студен описан следующим образом:
# class Student:
#     def __init__(self, name, money):
#         self.name = name
#         self.money = money
# Необходимо понять у кого больше всего денег и вернуть его имя.
# assert most_money([ivan, oleg, sergei]) == "sergei"
# assert most_money([ivan, oleg]) == "ivan"
# assert most_money([oleg]) == "Oleg"


class Student:
    def __init__(self, name: str, money: float):
        self.name = name
        self._money = money

    @property
    def money(self):
        return self._money

    @money.setter
    def money(self, value):
        self._money = value


def most_money(students):
    student_money = [student.money for student in students]
    max_money = max(student_money)
    if student_money.count(max_money) == len(student_money) and \
            len(students) != 1:
        return "Everyone has the same amount of money"
    else:
        for student in students:
            if student.money == max_money:
                return student.name


if __name__ == "__main__":
    sergei = Student("sergei", 300)
    ivan = Student("ivan", 200)
    oleg = Student("oleg", 100)

    assert most_money([ivan, oleg, sergei]) == "sergei"
    assert most_money([ivan, oleg]) == "ivan"
    assert most_money([oleg]) == "oleg"

    print(most_money([ivan, oleg, sergei]))
