import numpy as np

"""Генераторы:
1) Напишите функцию-генератор, которая вычисляет числа фибоначчи
"""


def fib_generator(n):
    a, b = 0, 1
    while a < n:
        yield a
        a, b = b, a + b


for i in fib_generator(1000):
    print(i)

"""
2) Напишите генератор списка который принимает список numbers = [34.6, -203.4,
44.9, 68.3, -12.2, 44.6, 12.7] и возвращает новый список только
с положительными числами
"""


def generate_from_list(some_list):
    new_list = [i for i in some_list if i > 0]
    yield new_list


numbers = [34.6, -203.4, 44.9, 68.3, -12.2, 44.6, 12.7]
for i in generate_from_list(numbers):
    print(i)

"""
3) Необходимо составить список чисел которые указывают на длину слов в строке:
sentence = "the quick brown fox jumps over the lazy dog", но
только если слово не "the".
"""


def count_of_letters_in_words(sentence="the quick brown"
                                       " fox jumps over the lazy dog"):
    words_list = list(sentence.split())
    for i in words_list:
        if i != 'the':
            yield len(i)


for i in count_of_letters_in_words():
    print(i)

"""
Петя перешел в другую школу. На уроке физкультуры ему понадобилось определить
своё место в строю. Помогите ему это сделать. Программа получает на вход не
возрастающую последовательность натуральных чисел, означающих рост каждого
человека в строю. После этого вводится число X – рост Пети.
Выведите номер, под которым Петя должен встать в строй. Если в строю
есть люди с одинаковым ростом,таким же, как у Пети, то он должен встать
после них. rank([165, 163, 162, 160, 157, 157, 155, 154], 162) #=> 3
"""


def some_function(list_of_height: list, own_height: int):
    list_of_height.append(own_height)
    list_of_height.sort(reverse=True)
    for i in range(len(list_of_height) - 1):
        if list_of_height[i] == own_height and \
                list_of_height[i + 1] != own_height:
            return i


rank = [165, 163, 162, 160, 157, 157, 155, 154]
print(some_function(rank, 162))

"""
Дан список чисел. Выведите все элементы списка, которые больше предыдущего
элемента.
[1, 5, 2, 4, 3] #=> [5, 4]
[1, 2, 3, 4, 5] #=> [2, 3, 4, 5]
"""


def more_than_previous_number(list_of_numbers):
    more_than = []
    for i in range(len(list_of_numbers) - 1):
        if list_of_numbers[i + 1] > list_of_numbers[i]:
            more_than.append(list_of_numbers[i + 1])

    return more_than


print(more_than_previous_number([1, 5, 2, 4, 3]))
print(more_than_previous_number([1, 2, 3, 4, 5]))

"""
Напишите программу, принимающую зубчатый массив любого типа и
возвращающего его "плоскую" копию.
List = [1, 2, [3, 4, [5, 6], 7], [8, [9, [10]]]]
"""


def build_normal_list(some_list):  # Догадываюсь, что есть какой-то
    a = []  # другой метод, но придумать луч-
    for i in some_list:  # чше - я не знаю =(
        if isinstance(i, list) or isinstance(i, tuple):
            for j in i:
                if isinstance(j, list) or isinstance(j, tuple):
                    for k in j:
                        if isinstance(k, list) or isinstance(k, tuple):
                            for z in k:
                                a.append(z)
                        else:
                            a.append(k)
                else:
                    a.append(j)
        else:
            a.append(i)

    return a


print(build_normal_list([1, 2, [3, 4, [5, 6], 7], [8, [9, [10]]]]))

"""
Напишите функцию, которая принимает на вход одномерный массив и два числа -
размеры выходной матрицы. На выход программа должна подавать матрицу нужного
размера, сконструированную из элементов массива.
reshape([1, 2, 3, 4, 5, 6], 2, 3) =>
[
[1, 2, 3],
[4, 5, 6]
]
reshape([1, 2, 3, 4, 5, 6, 7, 8,], 4, 2) =>
[
[1, 2],
[3, 4],
[5, 6],
[7, 8]
]"""


def build_matrix(some_list: list, height: int, width: int):
    a = np.array(some_list).reshape(height, width)
    return a


print(build_matrix([1, 2, 3, 4, 5, 6], 2, 3))
print(build_matrix([1, 2, 3, 4, 5, 6, 7, 8], 4, 2))
