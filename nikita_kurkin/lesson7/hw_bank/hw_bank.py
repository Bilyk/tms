"""
Написать функцию, которая вычисляет банковский вклад
Пользователь делает вклад в размере a рублей сроком на years
лет под 10% годовых (каждый год размер его вклада увеличивается
на 10%. Эти деньги прибавляются к сумме вклада, и на них в
следующем году тоже будут проценты). Написать функцию bank,
принимающая аргументы a и years, и возвращающую сумму,
которая будет на счету пользователя.
"""


def hw_bank(a: int, years: int) -> int:
    """
    :param a: how many money user want to take in a deposit in the bank
    :param years: how long will wait user
    :return: sum_ : exit sum after user will be take from deposit
    """
    sum_ = a
    percent = 0.1
    for i in range(years - 1):
        sum_ += sum_ * percent

    return sum_


if __name__ == "__main__":
    print(hw_bank(100000, 3))
