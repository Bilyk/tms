import unittest
from unittest.mock import patch
from book import Book, PageNumberError, PriceError, YearError


def mock_page(page):
    return page


def mock_author(author):
    return author


class TestBookCountOfPage(unittest.TestCase):
    def test_book_page_count_negative(self):
        with self.assertRaises(PageNumberError):
            Book.valid_count_pages(100)

    @patch('test_book.Book.valid_count_pages', side_effect=mock_page)
    def test_book_page_count_positive_mock(self, validate):
        result = Book.valid_count_pages(100)
        self.assertEqual(result, 100)


class TestBookAuthor(unittest.TestCase):
    @patch('test_book.Book.valid_author', side_effect=mock_author)
    def test_author_name_with_letter(self, mock):
        result = Book.valid_author('Rowling')
        print(result)
        self.assertEqual(result, 'Rowling')

    @unittest.expectedFailure
    def test_author_name_with_digits(self):
        self.assertEqual(Book.valid_author(123), False)


class TestBookYear(unittest.TestCase):
    def test_early_year(self):
        with self.assertRaises(YearError) as er:
            Book.valid_year(2000)
        self.assertEqual(er.exception.message, 'Wrong year')

    def test_old_year(self):
        self.assertEqual(Book.valid_year(1800), 1800)


class TestBookPrice(unittest.TestCase):
    def test_price_out_of_sequence(self):
        with self.assertRaises(PriceError):
            Book.valid_price(0)

    def test_price_into_sequence(self):
        self.assertEqual(Book.valid_price(200), 200)
