# 1. Свяжите переменную с любой строкой, состоящей не менее чем из 8 симв-ов
# Извлеките из строки первый символ, затем последний, третий с начала
# и третий с конца.Измерьте длину вашей строки.

str = "ilovecats"
str = str[1:]  # lovecats
str = str[:-1]  # lovecat
str = str[:1] + str[2:5] + str[6:7]  # lvecas
print(len(str))

# 2. Присвойте произвольную строку длиной 10-15 символов переменной
# и извлеките из нее следующие срезы:
# 1) первые восемь символов
# 2) четыре символа из центра строки
# 3) символы с индексами кратными трем
# 4) переверните строку

str = "ilovecatsverymuch"
srt_1 = str[:8]
middle = int((len(str) / 2))  # Находим центр строки
str_2 = str[middle - 2:middle + 2]  # Срезаем 4 символа из центра
str_3 = str[::3]
str_4 = str[::-1]

# 3. Есть строка: “my name is name”. Напечатайте ее,
# но вместо 2ого “name” вставьте ваше имя.

str = "My name is name"
# C помощью split/join
words = str.split()
words[-1] = "Ania"
print(' '.join(words))
# С помощью реплейс
print(str.replace("s name", "s Ania"))

# 4. Есть строка: test_tring = "Hello world!", необходимо
# - напечатать на каком месте находится буква w
# - кол-во букв l
# - Проверить начинается ли строка с подстроки: “Hello”
# - Проверить заканчивается ли строка подстрокой: “qwe”

str = "Hello world!"
print(str.index("w"))
print(str.count('l'))
print(str.startswith("Hello"))
print(str.endswith("qwe"))
