# Определить иерархию и создать несколько цветов (Rose, Tulips, violet,
# chamomile). У каждого класса цветка следующие атрибуты:
# Стоимость – атрибут класса, который определяется заранее
# Свежесть (в днях), цвет, длинна стебля – атрибуты экземпляра
# При попытке вывести информацию о цветке, должен отображаться цвет и тип

class Flowers:
    def __init__(self, name, days, color, length):
        self.name = name
        self.days = days
        self.color = color
        self.length = length

    def __str__(self):
        return f"{self.color} {self.name}"


class RoseClass(Flowers):
    price = 5


class TulipsClass(Flowers):
    price = 3


class VioletClass(Flowers):
    price = 2


class ChamomileClass(Flowers):
    price = 3


Rose1 = RoseClass('Rose', 3, 'Red', 25)
Tulip1 = TulipsClass('Tulip', 1, 'Yellow', 20)
Violet1 = VioletClass('Violet', 3, 'Purple', 15)
Chamomile1 = ChamomileClass('Chamomile', 5, 'White', 22)

# Можно использовать аксессуары – отдельные классы со своей стоимостью:
# например, упаковочная бумага)


class Wrapper:
    def __init__(self, material, price):
        self.material = material
        self.price = price


wrapper1 = Wrapper("Paper", 20)


class Bouquet:

    def __init__(self, *args):
        """Собрать букет"""
        self.all = list(args)
        self.flowers = []
        self.decor = []
        for i in args:
            if isinstance(i, Flowers):
                self.flowers.append(i)
            else:
                self.decor.append(i)

    def calculate_bouquet_amount(self):
        """ определение стоимости букета"""
        amount = sum([i.price for i in self.all])
        return amount

    def average_life_cycle(self):
        """определить время увядания букета по среднему времени жизни
        всех цветов в букете"""
        return int(sum([i.days for i in self.flowers]) / len(self.flowers))

    def sort_flowers_by_parameter(self, parameter):
        """Реализовать поиск цветов в букете по определенным параметрам."""
        if parameter == 'days':
            sorted_flowers = sorted(self.flowers, key=lambda x: x.days)
            print(f'Flowers sorted by {parameter} are', [i.name for i in
                                                         sorted_flowers])
        elif parameter == 'name':
            sorted_flowers = sorted(self.flowers, key=lambda x: x.name)
            print(f'Flowers sorted by {parameter} are',
                  [i.name for i in sorted_flowers])
        elif parameter == 'color':
            sorted_flowers = sorted(self.flowers, key=lambda x: x.color)
            print(f'Flowers sorted by {parameter} are',
                  [i.name for i in sorted_flowers])
        elif parameter == 'length':
            sorted_flowers = sorted(self.flowers, key=lambda x: x.length)
            print(f'Flowers sorted by {parameter} are',
                  [i.name for i in sorted_flowers])

    def search_flowers_by_parameter(self, parameter, criteria):
        """Позволить сортировку цветов в букете на основе различных параметров
         (свежесть/цвет/длина стебля/стоимость...)"""
        result = []
        if parameter == 'name':
            for i in self.flowers:
                if i.name == criteria:
                    result.append(i.name)
        elif parameter == 'days':
            for i in self.flowers:
                if i.days == criteria:
                    result.append(i.name)
        elif parameter == 'color':
            for i in self.flowers:
                if i.color == criteria:
                    result.append(i.name)
        elif parameter == 'length':
            for i in self.flowers:
                if i.length == criteria:
                    result.append(i.name)
        if result:
            print('Search results:', result)
        else:
            print("Sorry, we could not find flowers with your criteria")

    def __contains__(self, flower):
        """Узнать, есть ли цветок в букете. (__contains__)"""
        return flower in self.flowers

    def __getitem__(self, item):
        """Добавить возможность получения цветка по индексу (__getitem__)"""
        return self.flowers[item].name


b1 = Bouquet(Tulip1, wrapper1, Rose1, Violet1)

print(b1.calculate_bouquet_amount())
print(b1.average_life_cycle())

b1.sort_flowers_by_parameter('length')
b1.search_flowers_by_parameter('length', 15)
print(Chamomile1 in b1)
print(b1[1])
