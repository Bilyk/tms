# Task2: Like.
# Создайте программу, которая, принимая массив имён, возвращает строку
# описывающая количество лайков (как в Facebook)
# Введите имена через запятую: "Ann"
# -> "Ann likes this"
# Введите имена через запятую: "Ann, Alex"
# -> "Ann and Alex like this"
# Введите имена через запятую: "Ann, Alex, Mark"
# -> "Ann, Alex and Mark like this"
# Введите имена через запятую: "Ann, Alex, Mark, Max"
# -> "Ann, Alex and 2 others like this"
# Если ничего не вводить должен быть вывод:
# -> "No one likes this"
names = input('Введите имена через запятую: ')
if not names:
    print('No one likes this')
else:
    list_names = names.split(',')
    if len(list_names) == 1:
        print(f'{list_names[0]} likes this')
    elif len(list_names) == 2:
        print(f'{list_names[0]} and{list_names[1]} like this')
    elif len(list_names) == 3:
        print(f'{list_names[0]},{list_names[1]} and{list_names[2]} like this')
    elif len(list_names) > 3:
        print(f'{list_names[0]},{list_names[1]} and {len(list_names) - 2} '
              f'others like this')

# Task3: BuzzFuzz.
# Напишите программу, которая перебирает последовательность от 1 до 100.
# Для чисел кратных 3 она должна написать: "Fuzz" вместо печати числа, а для
# чисел кратных 5  печатать "Buzz". Для чисел которые кратны 3 и 5 надо
# печатать "FuzzBuzz". Иначе печатать число.
# Вывод должен быть следующим:
# 1
# 2
# fuzz
# 4
# buzz
# fuzz
# 7
# 8
# ..
for i in range(0, 101):
    if i % 3 == 0 and i % 5 != 0:
        print('Fuzz')
    elif i % 5 == 0 and i % 3 != 0:
        print('Buzz')
    elif i % 5 == 0 and i % 3 == 0:
        print('FuzzBuzz')
    else:
        print(i)

# Task4: Напишите код, который возьмет список строк и пронумерует их.
# Нумерация начинается с 1, имеет “:” и пробел
# [] => []
# ["a", "b", "c"] => ["1: a", "2: b", "3: c"]
lst1 = ['a', 'b', 'c']
index_lst1 = 0
numerate = 1
for i in lst1:
    lst1[index_lst1] = f'{numerate}: {i}'
    index_lst1 += 1
    numerate += 1
print('Task 4:', lst1)

# Task5: Проверить, все ли элементы одинаковые
# [1, 1, 1] == True
# [1, 2, 1] == False
# ['a', 'a', 'a'] == True
# [] == True
lst4 = ['a', 'a', 'a']
if len(lst4) == 0:
    print('Task 5:', True)
elif len(set(lst4)) == 1:
    print('Task 5:', True)
else:
    print('Task 5:', False)

# Task6: Проверка строки. В данной подстроке проверить все ли буквы в строчном
# регистре или нет и вернуть список не подходящих.
# dogcat => [True, []]
# doGCat => [False, ['G', 'C']]
str6 = 'doGCat'
str_not_lower = [False]
str_lower = [True]
unfit = []
if str6 == str6.lower():
    str_lower.append([])
    print('Task 6:', str_lower)
else:
    for i in str6:
        if i != i.lower():
            unfit.append(i)
    str_not_lower.append(unfit)
    print('Task 6:', str_not_lower)

# Task7: Сложите все числа в списке, они могут быть отрицательными, если
# список пустой вернуть 0
# [] == 0
# [1, 2, 3] == 6
# [1.1, 2.2, 3.3] == 6.6
# [4, 5, 6] == 15
# range(101) == 5050
lst7 = range(101)
if lst7:
    print('Task 7:', sum(lst7))
else:
    print('Task 7:', 0)
