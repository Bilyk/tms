def logging_decorator(func):
    """Decorator for logging elements found on the page."""
    def wrapper():
        print(f"Locating {func.__name__}...")
        func()
        print("Done.")
        return func()
    return wrapper
