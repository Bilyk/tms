import xml.etree.ElementTree as ET

xml_file = 'C:\\data\\HomeWork_25\\library.xml'

tree = ET.parse(xml_file)
root = tree.getroot()


def find_book_by_author(author_name: str):
    return list(filter(lambda child: child[0].text == author_name, root))


def find_book_by_price(price: str):
    return list(filter(lambda child: child[3].text == price, root))


def find_book_by_title(title: str):
    return list(filter(lambda child: title in child[1].text, root))


def find_book_by_description(text: str):
    return list(filter(lambda child: text in child[5].text, root))


if __name__ == "__main__":
    search = "XML"
    print(find_book_by_title(search))
