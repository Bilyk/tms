class InputTypeError(Exception):
    message = "Can not convert a string into a float"

    def __init__(self):
        super().__init__(self.message)


def convert_to_float(arg):
    """
    Converts a number passed as arg into a float.
     Raises an exception if arg is a string.
    :param arg: number to convert
    :return: float object
    """
    if isinstance(arg, str):
        raise InputTypeError
    else:
        return float(arg)
