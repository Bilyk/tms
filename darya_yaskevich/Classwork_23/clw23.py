from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

driver = webdriver.Chrome('C:\\data\\chromedriver_win32\\chromedriver.exe')


# # check radiobutton and verify it is checked
def check_radiobutton():
    try:
        driver.get('https://demoqa.com/radio-button')
        radio_button = driver.find_element(
            By.XPATH, '//label[text()="Impressive"]')
        radio_button.click()
        message = driver.find_element(By.CSS_SELECTOR, '.text-success')

        assert "Impressive" in message.text

    finally:
        driver.quit()


def iframes_task():
    try:
        driver.get('https://jsbin.com/cicenovile/1/edit?html,output')
        waiter = WebDriverWait(driver, 10)
        driver.switch_to.frame(driver.find_element(By.TAG_NAME, 'iframe'))
        waiter.until(EC.presence_of_element_located((By.TAG_NAME, 'iframe')))
        driver.switch_to.frame(driver.find_element(By.TAG_NAME, 'iframe'))
        driver.find_element(By.LINK_TEXT, "Click me!").click()
        alert = driver.switch_to.alert
        alert.accept()
        driver.switch_to.default_content()
        login_button = driver.find_element(By.ID, 'loginbtn')

        assert login_button.text == "Login or Register"

    finally:
        driver.quit()


if __name__ == "__main__":
    iframes_task()
