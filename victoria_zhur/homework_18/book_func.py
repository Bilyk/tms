"""
Создать класс Book. Атрибуты: количество страниц, год издания, автор, цена.
Добавить валидацию в конструкторе на ввод корректных данных.
Автор не должен содержать других символов кроме букв. Год не < 1980.
Количество страниц не > 4000. Цена не должна быть меньше 100 и больше 10000.
"""


class AuthorError(Exception):
    message = "Author name should contain only letters"

    def __init__(self):
        super().__init__(self.message)


class YearError(Exception):
    message = "Year should not be less than 1980"

    def __init__(self):
        super().__init__(self.message)


class PageAmountError(Exception):
    message = "Page amount should not be greater than 4000"

    def __init__(self):
        super().__init__(self.message)


class PriceError(Exception):
    message = "Price should in range 100 - 10000"

    def __init__(self):
        super().__init__(self.message)


class Book:
    def __init__(self, author, year, pages, price):
        self.author_name = self.validate_author(author)
        self.year = self.validate_year(year)
        self.pages = self.validate_pages(pages)
        self.price = self.validate_price(price)

    @staticmethod
    def validate_author(author):
        if author.isalpha():
            return author
        raise AuthorError

    @staticmethod
    def validate_year(year):
        if year < 1980:
            raise YearError
        return year

    @staticmethod
    def validate_pages(pages):
        if pages <= 4000:
            return pages
        raise PageAmountError

    @staticmethod
    def validate_price(price):
        if 100 <= price <= 10000:
            return price
        raise PriceError
