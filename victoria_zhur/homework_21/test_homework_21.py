from selenium.webdriver.common.by import By
from selenium.common.exceptions import StaleElementReferenceException

"""
Задание 1
На главной странице перейти по ссылке Checkboxes
Установить галочку для второго чекбокса и проверить что она установлена
Для первого чекбокса убрать галочку и протестировать, что ее нет
"""


def test_whether_checkbox_is_selected(driver, get_main_page):
    driver.find_element(By.LINK_TEXT, "Checkboxes").click()
    first_checkbox = driver.find_element(By.XPATH,
                                         '//*[@id="checkboxes"]/input[1]')
    second_checkbox = driver.find_element(By.XPATH,
                                          '//*[@id="checkboxes"]/input[2]')
    assert second_checkbox.is_selected()  # without any actions as the second
    # checkbox is selected by default
    assert not first_checkbox.is_selected()  # without any actions as the
    # first checkbox is not selected by default


"""
Задание 2
На главной странице перейти по ссылке Multiple Windows
После этого найти и нажать на ссылку Click here
Убедиться, что открылось новое окно и проверить что заголовок страницы –
New window
"""


def test_whether_new_window_is_opened(driver, get_main_page):
    driver.find_element(By.LINK_TEXT, "Multiple Windows").click()
    driver.find_element(By.CSS_SELECTOR, ".example > a").click()
    driver.switch_to.window(driver.window_handles[1])
    new_page_url = driver.current_url
    new_tab_title = driver.title
    assert new_page_url == "http://the-internet.herokuapp.com/windows/new"
    assert new_tab_title == "New Window"


"""
Задание 3
На главной странице перейти по ссылке Add/Remove elements
Проверить что при нажатии на кнопку Add element, появляется новый элемент
Написать отдельный тест, который проверяет удаление элементов
"""


def test_add_of_new_element(driver, get_main_page):
    driver.find_element(By.LINK_TEXT, "Add/Remove Elements").click()
    driver.find_element(By.CSS_SELECTOR, ".example > button").click()
    delete_button = driver.find_element(By.CSS_SELECTOR, "#elements > button")
    assert delete_button.is_displayed()


def test_removal_of_new_element(driver, get_main_page):
    driver.find_element(By.LINK_TEXT, "Add/Remove Elements").click()
    driver.find_element(By.CSS_SELECTOR, ".example > button").click()
    delete_button = driver.find_element(By.CSS_SELECTOR, "#elements > button")
    delete_button.click()
    try:
        assert not delete_button.is_displayed()
    except StaleElementReferenceException:
        pass
