from decorators_hw_8 import wrapper_with_words, incrementation_by_1,\
    convert_to_upper_case, func_name, change, timer


@wrapper_with_words
def just_print():
    print("Function itself")


@incrementation_by_1
def add_to_initial_number(n):
    return n


@convert_to_upper_case
def convertation(s: str):
    return s


@func_name
def func(arg):
    return arg


@change
def reverse_order(*args):
    return args


@timer
def performance(arg):
    return arg


@func_name
@timer
def fibonacci(n):
    """
    This function calculates Fibonacci numbers
    """
    fib1, fib2 = 0, 1
    for i in range(n):
        print(fib1)
        fib1, fib2 = fib2, fib1 + fib2


@wrapper_with_words
@incrementation_by_1
def complex_math_expression(x):
    a = (abs(x) / x * x) * (x**3)
    return a


def function_to_run():
    """This function provides interactive menu to select function to run"""
    while True:
        user_option = int(input("Enter the number of function to run it:"
                                "\n 1) function that wraps result with words"
                                "'Before' and 'After',"
                                "\n 2) function that implements incrementation"
                                "by 1,"
                                "\n 3) function that converts register of text"
                                "into upper case,"
                                "\n 4) function that prints name of the"
                                "function,"
                                "\n 5) function that reverses functions"
                                "arguments,"
                                "\n 6) function that calculates performance of"
                                "decorator function,"
                                "\n 7) function that calculates Fibonacci"
                                "number,"
                                "\n 8) function that performs complex math "
                                "expression "))
        if user_option == 1:
            just_print()
        elif user_option == 2:
            print(add_to_initial_number(7))
        elif user_option == 3:
            print(convertation("text in upper case"))
        elif user_option == 4:
            func(5)
        elif user_option == 5:
            print(reverse_order("a", "b", "c"))
        elif user_option == 6:
            print(performance(10000))
        elif user_option == 7:
            print(fibonacci(3))
        elif user_option == 8:
            complex_math_expression(-3)
        else:
            break


function_to_run()
