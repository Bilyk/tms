"""
Hero
В игре есть солдаты и герои. У каждого есть уникальный номер, и свойство,
в котором хранится принадлежность команде. У солдат есть метод "move_to_hero",
который в качестве аргумента принимает объект типа "Hero". У героев есть метод
увеличения собственного уровня level_up.
В основной ветке программы создается по одному герою для каждой команды.
В цикле генерируются объекты-солдаты. Их принадлежность команде определяется
случайно. Солдаты разных команд добавляются в разные списки.
Измеряется длина списков солдат противоборствующих команд и выводится на экран.
У героя, принадлежащего команде с более длинным списком, поднимается уровень.
Отправьте одного из солдат первого героя следовать за ним. Выведите на экран
идентификационные номера этих двух юнитов.
"""
import random


class Soldiers:
    id_ = 1

    def __init__(self):
        self.id_ = Soldiers.id_
        Soldiers.id_ += 1

    def go_hero(self, hero):
        print(f'солдат с id {self.id_} следует за героем {hero.id_}')

    def __str__(self):
        return f'{self.id_}'


class Heroes(Soldiers):

    def __init__(self, team):
        Soldiers.__init__(self)
        self.team = team
        self.level = 0

    def level_up(self):
        self.level += 1
        print(f'герой с id {self.id_} достиг {self.level} уровня')


if __name__ == "__main__":
    print(f"task_2 is {__name__}")
else:
    print(f"import {__name__}")
heroes_1 = Heroes('red')
heroes_2 = Heroes('blue')
list_heroes_1 = []
list_heroes_2 = []
for _ in range(20):
    if random.choice(['red', 'blue']) == 'red':
        list_heroes_1.append(Soldiers())
    else:
        list_heroes_2.append(Soldiers())

len_heroes_1 = len(list_heroes_1)
len_heroes_2 = len(list_heroes_2)
print(f'солдат у героя heroes_1 - {len_heroes_1}')
print(f'солдат у героя heroes_2 - {len_heroes_2}')
if len_heroes_1 > len_heroes_2:
    heroes_1.level_up()
else:
    heroes_2.level_up()

random.choice(list_heroes_1).go_hero(heroes_1)
