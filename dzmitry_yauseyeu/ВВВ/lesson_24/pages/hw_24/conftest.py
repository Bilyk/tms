import time

import pytest
from selenium import webdriver


@pytest.fixture(scope="session")
def driver():
    driver = webdriver.\
        Chrome(executable_path="C:/Dim/chromedriver_win32/chromedriver")
    yield driver
    time.sleep(2)
    driver.quit()
