from functools import wraps
from timeit import default_timer


def func_name(func):
    @wraps(func)
    def wrapper(*args):
        print(func(*args))
        print(func.__name__)
    return wrapper


def before_after(func):
    @wraps(func)
    def inner_function(*arg):
        print('Before')
        print(func(*arg))
        print('After')
    return inner_function


def increase_by_one(func):
    @wraps(func)
    def wrapper(*args):
        return func(*args) + 1
    return wrapper


def convert_to_uppercase(func):
    @wraps(func)
    def wrapper(arg):
        set_str_to_uppercase = arg.upper()
        return set_str_to_uppercase
    return wrapper


def change(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        reversed_args = args[::-1]
        return func(reversed_args, **kwargs)
    return wrapper


def time_measurement(func):
    @wraps(func)
    def wrapper(*args):
        start_time = default_timer()
        func(*args)
        # print(func())
        end_time = default_timer()
        return end_time - start_time
    return wrapper


def check_args_type(type='str'):
    def wrapper(func):
        @wraps(func)
        def inner(*args):
            list_of_args = [i for i in args]
            if type == 'str':
                concated_str = ''.join(str(el) for el in list_of_args)
                return concated_str
            elif type == 'int':
                if any(isinstance(el, float) for el in list_of_args):
                    list_of_floats = [float(i) for i in args]
                    return sum(list_of_floats)
                elif all(isinstance(el, int) for el in list_of_args):
                    list_of_ints = [int(i) for i in args]
                    return sum(list_of_ints)
                elif any(isinstance(el, str) for el in list_of_args):
                    return 'Допустимые значения параметров - int и float'
            else:
                return 'Введите str или int'
        return inner
    return wrapper
