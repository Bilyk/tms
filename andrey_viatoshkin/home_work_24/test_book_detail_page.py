from andrey_viatoshkin.home_work_24.pages.book_detail_page_locators import\
    BookDetailPageLocators


def test_book_has_header_displayed(driver, return_book_page):
    book_detail_page = return_book_page.find_book_by_name(
        "The shellcoder's handbook")
    assert "The shellcoder's handbook" == book_detail_page.get_header(
        BookDetailPageLocators.HEADER_SELECTOR)


def test_book_price(driver, return_book_page):
    book_detail_page = return_book_page.find_book_by_name(
        "The shellcoder's handbook")
    assert "£9.99" == book_detail_page.get_header(
        BookDetailPageLocators.PRICE_SELECTOR), 'Price should be £9.99'


def test_book_logo(driver, return_book_page):
    book_detail_page = return_book_page.find_book_by_name(
        "The shellcoder's handbook")
    assert book_detail_page.get_book_image(), 'Wrong header'
