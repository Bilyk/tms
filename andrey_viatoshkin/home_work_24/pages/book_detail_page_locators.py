from selenium.webdriver.common.by import By


class BookDetailPageLocators:
    HEADER_SELECTOR = (By.XPATH, '//h1')
    PRICE_SELECTOR = (By.XPATH), '//*[@class="price_color"]'
    BOOK_IMAGE_LOCATOR = (By.XPATH, '//img[contains(@alt,"The shellcoder")]')
