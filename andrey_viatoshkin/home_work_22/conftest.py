import pytest
from selenium import webdriver
from selenium.webdriver.common.by import By


URL = 'https://www.saucedemo.com/'
USERNAME_INPUT_CSS_SELECTOR = '[name="user-name"]'
PASSWORD_INPUT_CSS_SELECTOR = '[name="password"]'
LOGIN_BUTTON_CSS_SELECTOR = '[type="submit"]'


@pytest.fixture(scope="function")
def driver():
    driver = webdriver.Chrome()
    driver.get(URL)
    driver.find_element(By.CSS_SELECTOR, USERNAME_INPUT_CSS_SELECTOR)\
        .send_keys('standard_user')
    driver.find_element(By.CSS_SELECTOR, PASSWORD_INPUT_CSS_SELECTOR)\
        .send_keys('secret_sauce')
    driver.find_element(By.CSS_SELECTOR, LOGIN_BUTTON_CSS_SELECTOR).click()
    return driver
